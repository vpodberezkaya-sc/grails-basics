package com.scand.edu.controller

import com.scand.edu.Feedback
import com.scand.edu.FeedbackController
import com.scand.edu.FeedbackService
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(FeedbackController)
class FeedbackControllerSpec extends Specification {

    def feedbackService = Mock FeedbackService
    def feedback = Mock Feedback

    def setup() {
        controller.feedbackService = feedbackService
    }

    def cleanup() {
    }

    void "test index"() {
        given:
        params.offset = 0
        List<Feedback> feedbacks = []
        int messageCount

        when:
        def result = controller.index()

        then:
        1 * feedbackService.list(0, 3) >> feedbacks
        1 * feedbackService.totalNumber() >> messageCount
        0 * _

        and:
        result == [feedbacks: feedbacks, messageCount: messageCount, offset: 0, max: 3]
    }

    void "test addFeedback()"() {
        given:
        params.offset = 0
        List<Feedback> feedbacks = []
        int messageCount

        when:
        controller.addFeedback(feedback)

        then:
        1 * feedbackService.save(feedback) >> feedback
        1 * feedbackService.list(0, 3) >> feedbacks
        1 * feedbackService.totalNumber() >> messageCount
        0 * _

        and:
        model == [feedback: feedback, feedbacks: feedbacks, messageCount: messageCount, offset: 0, max: 3]
        view == '/feedback/index'
    }
}
