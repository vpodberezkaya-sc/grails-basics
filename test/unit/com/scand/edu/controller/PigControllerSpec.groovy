package com.scand.edu.controller

import com.scand.edu.FileSystemHelper
import com.scand.edu.ImageService
import com.scand.edu.Pig
import com.scand.edu.ItemPayload
import com.scand.edu.ImageCommand
import com.scand.edu.PigService
import com.scand.edu.PigController
import grails.test.mixin.TestFor
import org.codehaus.groovy.grails.plugins.testing.GrailsMockMultipartFile
import spock.lang.Specification

import java.nio.file.Paths

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(PigController)
class PigControllerSpec extends Specification {

    def pig = Mock Pig
    def pigPayload = Mock ItemPayload
    def pigService = Mock PigService
    def imageService = Mock ImageService
    def imageCommand = Mock ImageCommand
    def fileSystemHelper = Mock FileSystemHelper

    def setup() {
        controller.pigService = pigService
        imageService.fileSystemHelper = fileSystemHelper
        controller.imageService = imageService
    }

    def cleanup() {
    }

    void "test create()"() {
        when:
        controller.create()

        then:
        !controller.modelAndView
        !response.redirectedUrl
    }

    void "test search()"() {
        when:
        controller.search()

        then:
        model == [pigCount: 0, max: 3]
        view == '/pig/search'
    }

    void "test delete()"() {
        given:
        Long id = 1L

        when:
        controller.delete(id)

        then:
        1 * pigService.getById(id) >> pig
        1 * pigService.delete(pig)
        0 * _

        and:
        response.redirectedUrl == '/pig/index'
    }

    void "test index()"() {
        given:
        params.offset = '5'
        List<Pig> pigs = [pig]

        when:
        def result = controller.index()

        then:
        1 * pigService.list(5, 3) >> pigPayload
        1 * pigPayload.items >> pigs
        1 * pigPayload.itemCount >> 666
        0 * _

        and:
        result == [pigs: pigs, pigCount: 666, max: 3]
        !controller.modelAndView
        !response.redirectedUrl
    }

    void "test find()"() {
        given:
        params.searchQuery = "diggo"
        params.offset = 3
        params.breed = "cat"
        List<Pig> pigs = [pig]

        when:
        controller.find()

        then:
        1 * pigService.find("diggo", 3, 3) >> pigPayload
        1 * pigPayload.items >> pigs
        1 * pigPayload.itemCount >> 13
        0 * _

        and:
        model == [pigs: pigs, pigCount: 13, max: 3, breed: "cat", searchQuery: "diggo"]
        view == '/pig/search'
    }

    /*    def createPig(Pig pig, ImageCommand imageCommand) {


        pig.imagePath = Paths.get(pig.imageFolder, imageCommand.fileName) as String
        pigService.save(pig)
        if (!pig.hasErrors()) {
            ItemPayload pigPayload = pigService.list(params.offset, MAX_ON_PAGE)
            MultipartFile file = request.getFile(fileName)
            imageService.save(file, pig)
            render(view: 'index', model: [pig: pig, pigs: pigPayload.items, pigCount: pigPayload.itemCount, max: MAX_ON_PAGE])
        } else {
            render(view: 'create', model: [pig: pig])
        }
    }*/
    void "test createPig(Pig pig, ImageCommand imageCommand)"() {
        given:
        params.offset = 3
        def multipartFile = new GrailsMockMultipartFile("imageFile", new byte[0])
        request.addFile(multipartFile)
        List<Pig> pigs = [pig]
        String imagePath = Paths.get('pigFolder', 'imageFile') as String
        imageCommand.metaClass.validate = { true }
        pig.metaClass.validate = { true }

        when:
        controller.createPig(pig, imageCommand)

        then:
        1 * pigService.list(3, 3) >> pigPayload
        1 * imageCommand.fileName >> "imageFile"
        1 * pig.imageFolder >> "pigFolder"
        1 * pig.setImagePath("${imagePath}")
        1 * pigService.save(pig)
        1 * imageService.save(multipartFile, pig)
        1 * pigPayload.items >> pigs
        1 * pigPayload.itemCount >> 13
        1 * pig.hasErrors()
        0 * _

        and:
        view == '/pig/index'
        model == [pig: pig, pigs: pigs, pigCount: 13, max: 3]
    }


    void "test createPig(Pig pig, ImageCommand imageCommand): absent image(empty imageCommand) "() {
        given:
        params.offset = 3
        imageCommand.metaClass.validate = { false }

        when:
        controller.createPig(pig, imageCommand)

        then:
        view == '/pig/create'
        model == [pig: pig]
    }

    void "test createPig(Pig pig, ImageCommand imageCommand): invalid pig object"() {
        given:
        params.offset = 3
        pig.hasErrors()>>true
        def multipartFile = new GrailsMockMultipartFile("beauty", new byte[0])
        request.addFile(multipartFile)
        String imagePath = Paths.get('pigFolder', 'beauty') as String
        imageCommand.metaClass.validate = { true }

        when:
        controller.createPig(pig, imageCommand)


        then:
        1 * imageCommand.fileName >> "beauty"
        1 * pig.imageFolder >> "pigFolder"
        1 * pig.setImagePath(imagePath)
        1 * pigService.save(pig)

        and:
        view == '/pig/create'
        model == [pig: pig]
    }

}
