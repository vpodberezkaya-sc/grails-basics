package com.scand.edu.controller

import com.scand.edu.ImageController
import com.scand.edu.ImageDTO
import com.scand.edu.ImageService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(ImageController)
class ImageControllerSpec extends Specification {

    def imageService = Mock ImageService
    def image = Mock ImageDTO

    def setup() {
        controller.imageService = imageService
    }

    def cleanup() {
    }

    void "test getImage"() {
        given:
        params.imagePath = 'somePath'
        byte [] bytes = []

        when:
        controller.getImage()

        then:
        1*imageService.get('somePath') >> image
        1*image.image >> bytes
        1*image.fileType

//        and:
//        response.header('Content-length')=='jpg'
    }

    /* def getImage() {
        ImageDTO image = imageService.get(params.imagePath)
        byte[] imageInBytes = image.image
        String fileType = image.fileType

        response.with {
            setHeader('Content-length', imageInBytes.length.toString())
            contentType = "image/${fileType}"
            outputStream << imageInBytes
            outputStream.flush()
        }
    }*/
}
