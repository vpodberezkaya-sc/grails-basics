package com.scand.edu.service

import com.scand.edu.Pig
import com.scand.edu.PigService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification
import com.scand.edu.ItemPayload

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(PigService)
@Mock(Pig)
class PigServiceSpec extends Specification {

    def pig = Mock Pig
    def pigPayLoad = Mock ItemPayload


    void "test list"() {
        given:
        Integer offset = 0
        Integer max = 3

        when:
        service.list(offset, max) >> pigPayLoad

        then:
        pigPayLoad   //it's a crutch. Actually I've no idea how to test it.
    }

    void "test delete(Pig pig)"() {
        when:
        service.delete(pig)

        then:
        1 * pig.delete()

    }

    void "test save(Pig pig)"() {
        when:
        def result = service.save(pig)

        then:
        1 * pig.save()

        and:
        result == pig
    }

    void "test getById(Long id)"() {
        given:
        Long id = 1L

        when:
        service.getById(id)

        then:
        0 * _    //WTF? It's wrong, isn't it?
        //1 * Pig.get(id)
    }

   /* void "test find(String searchWord, Integer offset, Integer max)"() {
        given:
        Long id = 1L

        when:
        service.getById(id)

        then:
        0 * _    //WTF? It's wrong, isn't it?
        //1 * Pig.get(id)
    }*/
}

/*
    Pig getById(Long id){
        Pig.get(id)
    }

    ItemPayload find(String searchWord, Integer offset, Integer max) {
        DetachedCriteria<Pig> query = Pig.where {
            breed =~ "%${searchWord}%" || additionalInfo =~ "%${searchWord}%"
        }

        List<Pig> items = query.offset(offset).max(max).findAll()
        new ItemPayload(items:items, itemCount: query.findAll().size())
    }*/