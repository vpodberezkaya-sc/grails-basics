package com.scand.edu.service

import com.scand.edu.FeedbackService
import grails.test.mixin.TestFor
import spock.lang.Specification
import com.scand.edu.Feedback
/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(FeedbackService)
class FeedbackServiceSpec extends Specification {

    def feedback = Mock Feedback

    def setup() {
    }

    def cleanup() {
    }

    void "test save(Feedback feedback)"() {
        when:
        def result = service.save(feedback)

        then:
        result == feedback
    }

    void "test list(Integer offset, Integer max)"(){
        given:
        List<Feedback> feedbacks = [feedback]

        when:
        service.list(0,3) >> feedbacks

        then:
        feedbacks
    }

    void "test totalNumber()"(){
        given:
        int totalCount

        when:
        service.totalNumber() >> totalCount

        then:
        totalCount instanceof Integer && totalCount >= 0
    }
}

