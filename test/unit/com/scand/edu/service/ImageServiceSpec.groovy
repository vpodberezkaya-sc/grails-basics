package com.scand.edu.service

import com.scand.edu.FileSystemHelper
import com.scand.edu.ImageDTO
import com.scand.edu.ImageService
import com.scand.edu.Pig
import grails.test.mixin.TestFor
import org.springframework.mock.web.MockMultipartFile
import org.springframework.web.multipart.MultipartFile
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ImageService)
class ImageServiceSpec extends Specification {

    String sourceFolderPath = Paths.get(System.getProperty("user.dir"), 'test', 'unit', 'resources') as String

    def setup() {
        config.uploadFile = sourceFolderPath
        service.fileSystemHelper = new FileSystemHelper()
    }

    void "test get(String imagePath)"() {
        given:
        String imagePath = 'testFile.jpg'

        when:
        def result = service.get(imagePath)

        then:
        result instanceof ImageDTO
        result.fileType == 'jpg'
        result.image.size() == 106812
    }

    void "test save(MultipartFile file, Imageable imageable)"() {
        given:
        String imagePath = 'testFile.jpg'
        String contentType = "image/jpg"
        Path sourceImagePath = Paths.get(sourceFolderPath, imagePath)
        MultipartFile multipartFile = new MockMultipartFile(imagePath, imagePath, contentType, Files.readAllBytes(sourceImagePath))
        Pig testPig = new Pig(breed: 'lala', imagePath: imagePath)
        String savedFilePath = Paths.get(testPig.imageFolder, imagePath) as String

        when:
        service.save(multipartFile, testPig)

        then:
        service.get(savedFilePath)

        cleanup:
        def clean = Paths.get(sourceFolderPath, testPig.imageFolder)
        clean.deleteDir()
    }
}
