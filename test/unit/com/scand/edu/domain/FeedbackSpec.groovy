package com.scand.edu.domain

import com.scand.edu.Feedback
import grails.test.mixin.TestFor
import spock.lang.Specification


/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Feedback)
class FeedbackSpec extends Specification {

    void "test message cannot be null"() {
        when:
        domain.message = null

        then:
        !domain.validate(['message'])
        domain.errors['message'].code == 'nullable'
    }

    void "test authorName cannot be null"() {
        when:
        domain.message = null

        then:
        !domain.validate(['authorName'])
        domain.errors['authorName'].code == 'nullable'
    }

    void 'test message cannot be blank'() {
        when:
        domain.message = ''

        then:
        !domain.validate(['message'])
    }

    void 'test authorName cannot be blank'() {
        when:
        domain.authorName = ''

        then:
        !domain.validate(['authorName'])
    }

    void 'test message can have a maximum of 200 characters'() {
        when: 'for a string of 256 characters'
        String str = 'a' * 256
        domain.message = str

        then: 'message validation fails'
        !domain.validate(['message'])
        domain.errors['message'].code == 'size.toobig'

        when: 'for a string of 256 characters'
        str = 'a' * 199
        domain.message = str

        then: 'message validation passes'
        domain.validate(['message'])
    }
}

