package com.scand.edu.domain

import com.scand.edu.Pig
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Pig)
class PigSpec extends Specification {

    void "test breed cannot be null"() {
        when:
        domain.breed = null

        then:
        !domain.validate(['breed'])
        domain.errors['breed'].code == 'nullable'
    }

    void "test imagePath cannot be null"() {
        when:
        domain.imagePath = null

        then:
        !domain.validate(['imagePath'])
        domain.errors['imagePath'].code == 'nullable'
    }

    void 'test imagePath cannot be blank'() {
        when:
        domain.imagePath = ''

        then:
        !domain.validate(['imagePath'])
    }

    void 'test breed cannot be blank'() {
        when:
        domain.breed = ''

        then:
        !domain.validate(['breed'])
    }
}

/*    static constraints = {
        breed blank: false
        imagePath blank: false
    }*/
