package com.scand.edu

class OrderItem {
    Pig pig
    Integer quantity = 1
    OrderItemStatus status = OrderItemStatus.ORDERED

    static constraints = {
    }

    static belongsTo = [cart: ShoppingCart]
}
