package com.scand.edu

class Pig implements Imageable {

    static final String IMAGE_FOLDER = "pigs"  //hardcoded?

    String breed
    BigDecimal weight
    BigDecimal age
    String imagePath
    String additionalInfo

   // static belongsTo = [orderItem: OrderItem]

    static constraints = {
        breed blank: false
        weight nullable: true
        age nullable: true
        additionalInfo nullable: true
        imagePath blank: false
       // orderItem nullable: true
    }

    String getImageFolder() {
        IMAGE_FOLDER
    }
}
