package com.scand.edu

class Feedback {

    String authorName
    String message

    static constraints = {
        message size: 2..200 , blank: false
        authorName blank: false
    }

    String toString(){
        "${authorName}: ${message}"
    }
}
