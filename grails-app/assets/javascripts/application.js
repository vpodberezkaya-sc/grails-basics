// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//
//= require jquery
//= require_tree .
//= require_self

if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}

/*$( document ).ready( function() {
    $( ".quantity" ).change(function (){
        alert("${g.createLink( controller: 'shoppingCart', action:'setQuantity')}")
        $.ajax( {
            url: "${g.createLink( controller: 'shoppingCart', action:'setQuantity')}",
            type: "post",
            data:  {itemId:$('.orderItem'), quantity:$('.quantity' ).value()},
            success: function(resp){
                console.log(resp);}
        } );
    });
});*/

$(document).ready(function(){
    $(".quantity").on('change', function postinput(){
        var elem  = this
        alert(elem)
        console.log(this.value())
        $.ajax({
            url: '/Pigadise/shoppingCart/setQuantity',
            data: { itemId: 2, quantity: 5 },
            type: 'post'
        }).done(function(responseData) {
            console.log('Done: ', responseData);
        }).fail(function() {
            console.log('Failed');
        });
    });
});
