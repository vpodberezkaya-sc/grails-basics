<%--
  Created by IntelliJ IDEA.
  User: podberezkaya
  Date: 12/30/2018
  Time: 11:00 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<title>Registration</title>
	<meta name="layout" content="public"/>
</head>

<body>
<div class="container vlogo" style="width: 30em">
	<g:form controller="authentication" method="post">
		<input name="offset" type="hidden" value="${offset}"/>
		<div class="form-group ">
			<label for="firstName"><g:message code="registration.form.firstName"/></label>
		<div class="errorMessage">
			<g:textField name="firstName" class="form-control"/>
			<g:renderErrors bean="${user}" as="list" field="firstName" style="color: red"/>
		</div>
		<div class="form-group ">
			<label for="lastName"><g:message code="registration.form.lastName"/></label>
			<div class="errorMessage">
				<g:textField name="lastName" class="form-control"/>
				<g:renderErrors bean="${user}" as="list" field="lastName"/>
			</div>
		</div>
		<div class="form-group ">
			<label for="username"><g:message code="registration.form.userName"/></label>
			<div class="errorMessage">
				<g:textField name="username" class="form-control"/>
				<g:renderErrors bean="${user}" as="list" field="username"/>
			</div>
		</div>
		<div class="form-group ">
			<label for="email"><g:message code="registration.form.email"/></label>
			<div class="errorMessage">
				<g:textField name="email" class="form-control"/>
				<g:renderErrors bean="${user}" as="list" field="email"/>
			</div>
		</div>
		<div class="form-group ">
			<label for="password"><g:message code="registration.form.password"/></label>
			<div class="errorMessage">
				<g:passwordField  name="password" class="form-control"/>
				<g:renderErrors bean="${user}" as="list" field="password"/>
			</div>
		</div>
		<div class="form-group ">
			<label for="password"><g:message code="registration.form.repeatPassword"/></label>
			<div class="errorMessage">
				<g:passwordField  name="passwordRepeat" class="form-control"/>
				<g:renderErrors bean="${user}" as="list" field="password"/>
			</div>
		</div>
		<g:actionSubmit action="singUp" value="Sign up"  class="button btn-info  btn-lg" style="width: 100%"/>
	</g:form>
</div>
</div>
</body>

</html>

