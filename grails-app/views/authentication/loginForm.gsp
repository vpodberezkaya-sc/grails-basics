<%--
  Created by IntelliJ IDEA.
  User: podberezkaya
  Date: 12/30/2018
  Time: 10:12 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
	<head>
		<meta name="layout" content="public"/>
		<title>Login form</title>
	</head>
	<body>
		<div class="container vlogo" style="width: 30em">
			<form action="/Pigadise/j_spring_security_check" method="POST" id="loginForm" class="cssform" autocomplete="off">
				<p>
					<label for="username"><g:message code="login.form.userName"/></label>
					<input type="text" class="form-control" name="j_username" id="username">
				</p>

				<p>
					<label for="password"><g:message code="login.form.password"/></label>
					<input type="password" class="form-control" name="j_password" id="password">
				</p>

				<p id="remember_me_holder">
					<input type="checkbox" class="chk" name="_spring_security_remember_me" id="remember_me">
					<label for="remember_me"><g:message code="login.form.remember"/></label>
				</p>

				<p>
					<input type="submit" id="submit" value="Login" class="button btn-info  btn-lg" style="width: 100%">
				</p>
			</form>
		</div>
	</body>
</html>

