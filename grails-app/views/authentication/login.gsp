<%--
  Created by IntelliJ IDEA.
  User: podberezkaya
  Date: 12/27/2018
  Time: 4:31 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<title></title>
</head>

<body>
<div class="container vlogo">
	<g:form method="post">
		<input name="offset" type="hidden" value="${offset}"/>
		<div class="form-group ">
			<label for="authorName"><g:message code="feedback.message.authorName.label"/></label>
		<div class="errorMessage">
			<g:textField name="authorName" class="form-control"/>
			<g:renderErrors bean="${feedback}" as="list" field="authorName" style="color: red"/>
		</div>
		<div class="form-group ">
			<label for="message"><g:message code="feedback.message.message.label"/></label>
			<div class="errorMessage">
				<g:textArea name="message" class="form-control"/>
				<g:renderErrors bean="${feedback}" as="list" field="message"/>
			</div>
		</div>
		<g:actionSubmit action="addFeedback" value="add a feedback"/>
	</g:form>
</div>
</body>
</html>