<%@ page contentType="text/html;charset=UTF-8" %>
<html>
	<head>
		<meta name="layout" content="public"/>
		<title></title>
	</head>
	<body>
		<div class="container vlogo">
			<g:form method="post">
				<input name="offset" type="hidden" value="${offset}"/>
				<div class="form-group ">
					<label for="authorName"><g:message code="feedback.message.authorName.label"/></label>
				<div class="errorMessage">
					<g:textField name="authorName" class="form-control"/>
					<g:renderErrors bean="${feedback}" as="list" field="authorName" style="color: red"/>
				</div>
				<div class="form-group ">
					<label for="message"><g:message code="feedback.message.message.label"/></label>
					<div class="errorMessage">
						<g:textArea name="message" class="form-control"/>
						<g:renderErrors bean="${feedback}" as="list" field="message"/>
					</div>
				</div>
				<g:actionSubmit action="addFeedback" value="add a feedback"/>
			</g:form>
			<br>
			<hr>
			<div class="vlogor">
				<div class="headers-region"><h4 align="center" style="margin: 1.5em"><g:message code="feedback.label"/></h4>
				</div>
				<ul class="parent">
					<g:each in="${feedbacks}" var="feedback">
						<li>
							<article class="stranger" style="margin-left: 4em">
								<div>
									<footer>
										<b>${feedback.authorName}</b>
									</footer>
								</div>
								<div>
									${feedback.message}
								</div>
							</article>
						</li>
					</g:each>
					<li style="margin-left: 4em;">
					<g:paginate controller="feedback" action="index" total="${messageCount ?: 0}" max="${max}"/></li>
				</ul>
			</div>
		</div>
	</body>
</html>