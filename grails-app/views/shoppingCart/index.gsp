<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="public"/>
        <title>Welcome to Farm</title>
        <asset:javascript src="application.js"/>
    </head>
    <body>
        <br>
        <div class="container">
            <table id="table" class="table ">
            <tr style="border-top: hidden">
                <th><g:message code="pig.table.picture.col"/></th>
                <th><g:message code="pig.table.breed.col"/></th>
                <th><g:message code="shoppingCart.item.quantity"/></th>
            </tr>
                <g:each in="${items}" var="orderItem">
                    <tr class="pigs">
                        <td><img
                            src="${createLink(controller: 'image', action: "getImage", params: [imagePath: "${orderItem.pig.imagePath}"])}"
                            alt="${orderItem.pig.breed}" style="width: 7em; height: 5em"/>
                        </td>
                        <td style="align-content: center">
                            ${orderItem.pig.breed}
                        </td>
                        <td>
                            <input name="offset" type="hidden" value="${offset}"/>
                            <input name="orderItem" type="hidden" value="${orderItem.id}"/>
                            <input class="text quantity" type="text" maxlength="3" style="display: inline; width: 3em" value="${orderItem.quantity}"/>
                        </td>
                        <td>
                            <a class="button btn-info  btn-lg"
                               href="${createLink(action: 'removeFromCart', controller: 'shoppingCart', params: [orderItem_id: "${orderItem.id}"])}"><g:message code="shoppingCart.item.button.remove"/></a>
                        </td>
                    <tr/>
                </g:each>
            </table>
            <div class="row">
                <div class="col-sm-3 paging">
                    <g:paginate controller="shoppingCart" action="index" total="${pigCount}" max="${max}"/>
                </div>
                <div class="col-sm-6">
                </div>
                <div class="col-sm-3">
                    <a class="btn btn-lg pull-right" style="border: solid 1px" href="${createLink(action: 'checkout', controller: 'shoppingCart')}">
                        <g:message code="shoppingCart.item.button.checkout"/>
                    </a>
                </div>
            </div>
        </div>

    </body>
</html>
