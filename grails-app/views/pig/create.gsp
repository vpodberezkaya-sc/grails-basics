<%--
  Created by IntelliJ IDEA.
  User: podberezkaya
  Date: 11/19/2018
  Time: 4:25 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <meta name="layout" content="public"/>
        <title></title>
    </head>
    <body>
        <div class="container border feedback-form">
            <g:uploadForm action="createPig">
                <div class="form-group">
                    <label for="breed"><g:message code="feedback.message.breed.label"/></label>
                    <div class="errorMessage">
                    <g:textField name="breed" class="form-control" />
                    <g:renderErrors bean="${pig}" as="list" field="breed"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="breed"><g:message code="feedback.message.addInfo.label"/></label>
                    <g:textField name="additionalInfo" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="image"><g:message code="feedback.message.upload.label"/></label>
                    <div class="errorMessage">
                        <label class=" button btn-info  btn-lg">
                        <input id="image" type="file" class=".form-control-file form-control-lg" style="padding: 0" name="imageFile" hidden/>
                            Browse

                        </label><g:renderErrors bean="${pig}" as="list" field="imagePath"/>
                    </div>
                </div>
                <input type="submit" value="<g:message code="pig.message.upload"/>" class="button btn-info  btn-lg">
            </g:uploadForm>
        </div>
    </body>
</html>

