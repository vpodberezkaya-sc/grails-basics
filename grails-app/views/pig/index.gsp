<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="public"/>
        <title>Welcome to Farm</title>
    </head>
    <body>
        %{--<h4 align="center"><g:message code="index.message.welcome"/> </h4>--}%
        <br>
        <div class="container" style="width: 30em">
            <g:form controller="pig" action="find" method="GET">
                <div class="form-group">
                    <g:textField name="searchQuery" value = "${searchQuery}" class="form-control"/>
                </div>
                <input type="submit" value="<g:message code="index.linkName.search"/>">
            </g:form>
            <g:hasErrors bean="${message}">
                <g:eachError>
                    <p style="color:red;">
                        <g:message error="${it}"/>
                    </p>
                </g:eachError>
            </g:hasErrors>
        </div>
        %{--<br>   stub!!!--}%
        <br>
        <br>
        <div class="container">
            <table id="table" class="table ">
            <tr style="border-top: hidden">
                <th><g:message code="pig.table.picture.col"/></th>
                <th><g:message code="pig.table.breed.col"/></th>
                <th><g:message code="pig.table.info.col"/></th>
            </tr>
                <g:each in="${pigs}" var="pig">
                    <tr class="pigs">
                        <td><img
                                src="${createLink(controller: 'image', action: "getImage", params: [imagePath: "${pig.imagePath}"])}"
                                alt="${pig.breed}" style="width: 7em; height: 5em"/>
                        </td>
                        <td style="align-content: center">
                            ${pig.breed}
                        </td>
                        <td style="align-content: center">
                            ${pig.additionalInfo}
                        </td>
                        <sec:ifAnyGranted roles ="ROLE_ADMIN">
                        <td>
                            <input name="offset" type="hidden" value="${offset}"/>
                            <a class="button btn-info  btn-lg"
                               href="${createLink(action: 'delete', controller: 'pig', params: [pigId: "${pig.id}"])}"><g:message code="button.message.delete"/> </a>
                        </td>


                            <td>
                                <input name="offset" type="hidden" value="${offset}"/>
                                <a class="button btn-info  btn-lg"
                                   href="${createLink(action: 'update', controller: 'pig', params: [pigId: "${pig.id}"])}"><g:message code="button.message.update"/> </a>
                            </td>
                        </sec:ifAnyGranted>
                        %{--ifNotGranted--}%
                    <sec:ifLoggedIn>
                        <sec:ifNotGranted roles="ROLE_ADMIN" >
                        <td>
                            <a class="button btn-info  btn-lg"
                               href="${createLink(action: 'addToCart', controller: 'ShoppingCart', params: [pigId: "${pig.id}"])}"><g:message code="button.message.addToCart"/></a>
                        </td>
                        </sec:ifNotGranted>
                    </sec:ifLoggedIn>
                    <tr/>
                </g:each>
            </table>
            <div class="row">
                <div class="col-sm-3 paging">
                    <g:paginate controller="pig" action="index" total="${pigCount}" max="${max}"/>
                </div>
                <div class="col-sm-6">

                </div>
                <sec:ifAnyGranted roles ="ROLE_ADMIN">
                    <div class="col-sm-3">
                        <a class="btn btn-lg pull-right" style="border: solid 1px" href="${createLink(action: 'create', controller: 'pig')}">
                            <g:message code="pig.message.add"/>
                        </a>
                    </div>
                </sec:ifAnyGranted>
            </div>
        </div>

    </body>
</html>
