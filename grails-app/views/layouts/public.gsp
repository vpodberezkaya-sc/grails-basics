<!doctype html>
<html lang="en" class="no-js">
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	    <title>
	        <g:layoutTitle/>
	    </title>
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	    <meta name="viewport" content="width=device-width, initial-scale=1"/>
	    <asset:stylesheet src="application.css"/>
	    <g:layoutHead/>
	</head>

	<body>
		<nav>
		    <nav class="navbar navbar-expand-lg navbar-light">
		        <div class="container">
		            <div class="navbar-header">
		                <g:link controller="Pig" action=" ">
		                    <g:message code="index.linkName.pigs"/>
		                </g:link>
		            </div>
		            <div class="navbar-header">
		                <g:link controller="Feedback" action=" ">
		                    <g:message code="index.linkName.feedbacks"/>
		                </g:link>
		            </div>
		            <div class="navbar-header">
		                <a href="/Pigadise" >
		                    <g:message code="index.linkName.main"/>
		                </a>
		            </div>
			        <sec:ifLoggedIn>
				        <g:link controller='logout'>
					        <g:message code="navbar.logout"/>
				        </g:link>
			        </sec:ifLoggedIn>
			        <sec:ifNotLoggedIn>
				        <g:link controller='authentication' action="index">
					        <g:message code="navbar.login"/>
				        </g:link>
			        </sec:ifNotLoggedIn>
			        <sec:ifLoggedIn>
				        <div class="navbar-header">
					        <g:link controller="ShoppingCart" action=" ">
						        <span class="fas fa-shopping-cart"></span>
						        <g:message code="index.linkName.cart"/>
					        </g:link>
				        </div>
			        </sec:ifLoggedIn>
			        <sec:ifNotLoggedIn>
				        <g:link controller='authentication' action="showRegisterForm">
					       Register
				        </g:link>
			        </sec:ifNotLoggedIn>
		        </div>
		    </nav>
		</nav>
		<div class="vlog">
		    <asset:image class="heady" src="this.jpg"/>
		</div>
		<br>
		    <g:layoutBody/>
		<footer class="footer">
		</footer>
	</body>

</html>

