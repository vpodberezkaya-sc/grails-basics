<!DOCTYPE html>
<html>
    <head>
        <title>Welcome to Farm</title>
        <meta name="layout" content="public"/>
    </head>
    <body>
            <div class="container">
                <h1 class="welcomeHeader" align="center"><g:message code="index.greeting"/></h1>
                <h3><g:message code="index.dearCustomer"/></h3>
                <p><g:message code="index.welcomeMessage"/></p>
            </div>
    </body>
</html>
