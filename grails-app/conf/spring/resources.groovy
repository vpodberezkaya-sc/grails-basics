import com.scand.edu.FileSystemHelper


// Place your Spring DSL code here
beans = {
    fileSystemHelper(FileSystemHelper){ bean ->
        bean.autowire = 'byName'
    }
}


//<bean id=”myBean” class=”my.company.MyBeanImpl”></bean>