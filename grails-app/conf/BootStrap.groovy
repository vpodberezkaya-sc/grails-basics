import com.scand.edu.Feedback
import com.scand.edu.Pig
import com.scand.edu.ShoppingCart
import com.scand.edu.User
import com.scand.edu.UserRole
import com.scand.edu.Role

class BootStrap {

    def init = { servletContext ->
        new Feedback(authorName: 'John', message: 'Nice pork!').save()
        new Feedback(authorName: 'Jane', message: 'Ugly items').save()
        new Pig(breed: "piggo", imagePath: '\\pigs\\pig.jpg', additionalInfo: "Beautiful cat").save()
        new Pig(breed: "diggo", imagePath: '\\pigs\\Hereford Pig.jpg', additionalInfo: "Ugly dog").save()
        new Pig(breed: "diggo", imagePath: '\\pigs\\pig.jpg').save()
        new Pig(breed: "diggo", imagePath: '\\pigs\\Tamworth-Pig.png').save()
        new Pig(breed: "diggo", imagePath: '\\pigs\\juju.jpg').save()
        new Pig(breed: "diggo", imagePath: '\\pigs\\pigf.jpg').save()
        new Pig(breed: "diggo", imagePath: '\\pigs\\breed.jpg').save()
        new Pig(breed: "diggo", imagePath: '\\pigs\\ppn.png').save()
        new Pig(breed: "diggo", imagePath: '\\pigs\\pig.jpg').save()
        new User(username: "Jane", password: "superpuper", cart: new ShoppingCart(), email: "jane@mail.com",
        firstName: "Jane", lastName: "Smith").save()
        new User(username: "Joe", password: "superpuper", cart: new ShoppingCart(), email: "joe@mail.com",
                firstName: "Joe", lastName: "Doe").save()
        new User(username: "Jake", password: "superpuper", cart: new ShoppingCart(), email: "drake@mail.com",
                firstName: "Jake", lastName: "Drake").save()
        User user = new User(username: "Admin", password: "superpuper", cart: new ShoppingCart(), email: "admin@mail.com",
                firstName: "Admin", lastName: "Admin").save()
        Role role = new Role(authority: "ROLE_ADMIN").save()
        UserRole.create(user, role, true)

    }
    def destroy = {
    }
}
