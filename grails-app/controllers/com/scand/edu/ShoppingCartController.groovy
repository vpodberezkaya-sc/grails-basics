package com.scand.edu

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

@Secured(['isAuthenticated()'])
class ShoppingCartController {

    private final Integer MAX_ON_PAGE = 3


    OrderItemService orderItemService
    PigService pigService
    ShoppingCartService shoppingCartService
    SpringSecurityService springSecurityService

    def index() {
        Integer offset = params.offset as Integer ?: 0
        ItemPayload itemPayload = orderItemService.list(offset,MAX_ON_PAGE,  shoppingCartService.getByUser(getPrincipal()))
        [items: itemPayload.items, pigCount: itemPayload.itemCount, max: MAX_ON_PAGE]
    }

    def addToCart(Long pigId) {
        Pig pig = pigService.getById(pigId)
        ShoppingCart cart = shoppingCartService.getByUser(getPrincipal())
        OrderItem orderItem =orderItemService.save(cart, pig)//checkIfContained(ShoppingCart cart, Pig pig)
        shoppingCartService.addToCart(getPrincipal(), orderItem)
        redirect action: 'index', controller: "Pig"
    }

    def removeFromCart(Long orderItem_id) {
        OrderItem orderItem = orderItemService.getById(orderItem_id)
        orderItemService.delete(orderItem)
        redirect action: 'index'
    }

    def setQuantity() {
        println("!!!!!!!!!!!!!!!!!!!!!!!!!!!setQuantity is called!!!")
        OrderItem orderItem = orderItemService.getById(params.long("itemId"))
        orderItemService.setQuantity(orderItem, params.quantity as Integer)
    }

    def checkout() {
        shoppingCartService.checkout(getPrincipal())
        redirect action: 'index'
    }

    private User getPrincipal() {
        User.get(springSecurityService.principal.id)
    }
}

