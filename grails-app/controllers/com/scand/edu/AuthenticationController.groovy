package com.scand.edu

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

@Secured(['permitAll'])
class AuthenticationController {

    UserService userService
    SpringSecurityService springSecurityService

    def index() {
        render view: 'loginForm'
    }

    def login() {
        redirect(controller: "Pig", action: "index")
    }

    def logout() {

    }

    def showRegisterForm() {
        render view: 'registrationForm'
    }

    def singUp(User user, String passwordRepeat) {
        if (user.password && user.password != passwordRepeat) {
            user.errors.rejectValue(
                    'password',
                    'user.password.doesnotmatch')
        }
        if (user.password == passwordRepeat) {
            userService.create(user)
        }
        if (user && !user.hasErrors()) {
            springSecurityService.reauthenticate user.username
            redirect(controller: "Pig", action: "index")
        } else {
            render(view: 'registrationForm', model: [user: user])
        }

    }
}
