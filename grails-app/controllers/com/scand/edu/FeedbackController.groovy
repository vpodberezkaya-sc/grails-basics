package com.scand.edu

import com.scand.edu.Feedback
import com.scand.edu.FeedbackService
import grails.plugin.springsecurity.annotation.Secured

@Secured(['permitAll'])
class FeedbackController {

    private static final Integer MAX_ON_PAGE = 3

    FeedbackService feedbackService

    def index() {
        Integer offset = params.offset as Integer ?: 0
        List<Feedback> feedbacks = feedbackService.list(offset, MAX_ON_PAGE)
        Integer messageCount = feedbackService.totalNumber()
        [feedbacks: feedbacks, messageCount: messageCount, offset: offset, max: MAX_ON_PAGE]
    }

    def addFeedback(Feedback feedbackMessage) {
        Integer offset = params.offset as Integer ?: 0
        Feedback feedback = feedbackService.save(feedbackMessage)
        List<Feedback> feedbacks = feedbackService.list(offset, MAX_ON_PAGE)
        Integer messageCount = feedbackService.totalNumber()
        render(view: 'index', model: [feedback: feedback, feedbacks: feedbacks, messageCount: messageCount, offset: offset, max: MAX_ON_PAGE])
    }
}
