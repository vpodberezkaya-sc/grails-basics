package com.scand.edu

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.web.multipart.MultipartFile

import java.nio.file.Paths

@Secured(['permitAll'])
class PigController {

    private final Integer MAX_ON_PAGE = 3
    String fileName = "imageFile"   //it is the name of the input field for the image on the view;
    // it is used in createPig(..) method to retrieve the imageFile

    PigService pigService
    ImageService imageService
    SpringSecurityService springSecurityService

    def index() {
        Integer offset = params.offset as Integer ?: 0
        ItemPayload pigPayload = pigService.list(offset, MAX_ON_PAGE)
       // User principal = springSecurityService.principal?.id  //what if it's null?!!!
        [pigs: pigPayload.items, pigCount: pigPayload.itemCount, max: MAX_ON_PAGE]
    }

    def delete(Long id) {
        Pig pig = pigService.getById(id)
        pigService.delete(pig)
        redirect action: 'index', method: 'GET'
        //render(view: 'index', model: [items: pigPayload.items, pigCount: pigPayload.itemCount, offset: offset, max: MAX_ON_PAGE])
    }

    def search() {
        render(view: 'search', model: [pigCount: 0, max: MAX_ON_PAGE])
    }

    def find() {
        String searchQuery = params.searchQuery
        Integer offset = params.offset as Integer ?: 0
        ItemPayload pigLoad = pigService.find(searchQuery, offset, MAX_ON_PAGE)
        render(view: 'index', model: [pigs: pigLoad.items, pigCount: pigLoad.itemCount, max: MAX_ON_PAGE, breed: params.breed, searchQuery: searchQuery])
    }

    def create() {
    }

    def update(){

    }

    def createPig(Pig pig, ImageCommand imageCommand) {
        if (!imageCommand.validate()) {
            render(view: 'create', model: [pig: pig])
            return
        }

        pig.imagePath = Paths.get(pig.imageFolder, imageCommand.fileName) as String
        pigService.save(pig)
        if (!pig.hasErrors()) {
            ItemPayload pigPayload = pigService.list(params.offset, MAX_ON_PAGE)
            MultipartFile file = request.getFile(fileName)
            imageService.save(file, pig)
            render(view: 'index', model: [pig: pig, pigs: pigPayload.items, pigCount: pigPayload.itemCount, max: MAX_ON_PAGE])
        } else {
            render(view: 'create', model: [pig: pig])
        }
    }
}
