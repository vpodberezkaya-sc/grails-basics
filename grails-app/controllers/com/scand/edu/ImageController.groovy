package com.scand.edu

import grails.plugin.springsecurity.annotation.Secured

@Secured(['permitAll'])
class ImageController {

    ImageService imageService

    def getImage() {
        ImageDTO image = imageService.get(params.imagePath)
        byte[] imageInBytes = image.image
        String fileType = image.fileType

        response.with {
            setHeader('Content-length', imageInBytes.length.toString())
            contentType = "image/${fileType}"
            outputStream << imageInBytes
            outputStream.flush()
        }
    }
}
