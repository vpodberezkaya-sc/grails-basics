package com.scand.edu

import grails.transaction.Transactional

@Transactional
class UserService {

    User create(User user) {
        user.setCart( new ShoppingCart())
        user.save()
    }
}
