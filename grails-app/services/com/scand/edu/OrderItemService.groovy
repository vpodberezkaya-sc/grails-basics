package com.scand.edu

import grails.transaction.Transactional

@Transactional
class OrderItemService {

    OrderItem save (ShoppingCart cart, Pig pig) {
        OrderItem item = OrderItem.findByCartAndPig(cart, pig)
       if (item) {
            item.quantity++
        }else{
            item = new OrderItem(cart: cart, pig: pig)
        }
        item.save()
        item
    }

    ItemPayload list(Integer offset, Integer max, ShoppingCart cart) {
        List result = OrderItem.findAllByCartAndStatus(cart, OrderItemStatus.ORDERED, [offset: offset, max: max])
        new ItemPayload(items: result, itemCount: OrderItem.findAllByCartAndStatus(cart, OrderItemStatus.ORDERED).size())
    }

    void delete(OrderItem orderItem1) {
        orderItem1.delete(flush: true)
    }

    OrderItem getById(Long id) {
        OrderItem.get(id)
    }

    OrderItem setQuantity(OrderItem orderItem1, Integer quantity) {
        orderItem1.quantity = quantity
        orderItem1.save()
    }

    OrderItem checkIfContained(ShoppingCart cart, Pig pig) {
        OrderItem item = OrderItem.findByCartAndPig(cart, pig)
        if (item) {
            item.quantity++
            item.save()
        }
        item
    }
}
