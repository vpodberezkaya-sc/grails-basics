package com.scand.edu

import grails.gorm.DetachedCriteria


import grails.transaction.Transactional

@Transactional
class PigService {

    ItemPayload list(Integer offset, Integer max) {
        new ItemPayload(items: Pig.list(offset: offset, max: max), itemCount: Pig.count())
    }

    void delete(Pig pig) {
        pig.delete()
    }

    Pig getById(Long id){
        Pig.get(id)
    }

    Pig save(Pig pig) {
        pig.save()
        pig
    }

    ItemPayload find(String searchWord, Integer offset, Integer max) {
        DetachedCriteria<Pig> query = Pig.where {
            breed =~ "%${searchWord}%" || additionalInfo =~ "%${searchWord}%"
        }

        List<Pig> pigs = query.offset(offset).max(max).findAll()
        new ItemPayload(items:pigs, itemCount: query.findAll().size())
    }
}
