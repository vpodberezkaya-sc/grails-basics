package com.scand.edu

import grails.transaction.Transactional

@Transactional
class FeedbackService {

    Feedback save(Feedback feedback) {
        feedback.save()
        feedback
    }

    List<Feedback> list(Integer offset, Integer max){
       Feedback.list(offset: offset, max: max)
    }

    Integer totalNumber(){
        Feedback.count()
    }
}
