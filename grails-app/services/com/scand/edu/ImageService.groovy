package com.scand.edu

import grails.transaction.Transactional
import org.springframework.web.multipart.MultipartFile

import javax.imageio.ImageIO
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

@Transactional
class ImageService {

    FileSystemHelper fileSystemHelper

    ImageDTO get(String imagePath) {
        String fullImagePath = Paths.get(fileSystemHelper.IMAGE_ROOT_PATH, imagePath) as String
        int start = imagePath.lastIndexOf('.') + 1
        String fileType = imagePath.substring(start)
        ByteArrayOutputStream baos = new ByteArrayOutputStream()
        ImageIO.write(ImageIO.read(new File(fullImagePath)), fileType, baos)
        return new ImageDTO(image: baos.toByteArray(), fileType: fileType)
    }

    void save(MultipartFile file, Imageable imageable) {
        Path folderPath = Files.createDirectories(Paths.get(fileSystemHelper.IMAGE_ROOT_PATH, imageable.imageFolder))
        if (file && !file.empty) {
            file.transferTo(new File(folderPath.toFile(), file.originalFilename))
        }
    }
}
