package com.scand.edu

import grails.transaction.Transactional

@Transactional
class ShoppingCartService {

    def addToCart(User user, OrderItem item) {
        ShoppingCart.findByUser(user).addToItems(item)
    }

    ShoppingCart getByUser(User user) {
        ShoppingCart.findByUser(user)
    }

    def removeFromCart(User user, OrderItem item) {
        ShoppingCart.findByUser(user).removeFromItems(item)
    }

    def checkout(User user) {
        ShoppingCart cart = ShoppingCart.findByUser(user)
        cart.getItems().findAll { it -> it.status == OrderItemStatus.ORDERED }.each { it -> it.status = OrderItemStatus.PAID }
        //cart.items.each {it -> it.status = OrderItemStatus.PAID}
    }

    boolean containsPig(User user, Pig pig){
        ShoppingCart.findByUser(user)
    }

    ItemPayload list(User user) {
        //OrderItem.list(offset: offset, max: max)
        Set items = ShoppingCart.findByUser(user).getItems().findAll { it -> it.status == OrderItemStatus.ORDERED }
        new ItemPayload(items: items.toList(), itemCount: items.size())
    }
}
