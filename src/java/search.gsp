

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <meta name="layout" content="public"/>
        <title></title>
    </head>

    <body>
        <br>
    <div class="container" style="width: 60em">
	    <div class="container" style="width: 60%">
        <g:form controller="pig" action="find" method="GET">
            <div class="form-group">
                <g:textField name="searchQuery" class="form-control"/>
            </div>
            <input type="submit" value="<g:message code="index.linkName.search"/>">
        </g:form>
        <g:hasErrors bean="${message}">
            <g:eachError>
                <p style="color:red;">
                    <g:message error="${it}"/>
                </p>
            </g:eachError>
        </g:hasErrors>
	    </div>
	    <g:if test="${pigs != null}">
        <br>
        <h4 align="center">
	         <g:message code="search.result"/>
        </h4>
		    <table class="table" >
	        <tr style="border-top: hidden">
		        <th><g:message code="pig.table.picture.col"/></th>
		        <th><g:message code="pig.table.breed.col"/></th>
		        <th><g:message code="pig.table.info.col"/></th>
	        </tr>
            <g:each in="${pigs}" var="pig">
                <tr class="pigs">
                    <td><img
                            src="${createLink(controller: 'image', action: "getImage", params: [imagePath: "${pig.imagePath}"])}"
                            alt="${pig.breed}" style="width: 7em; height: 5em"/>
                    </td>
                    <td style="align-content: center">
                        ${pig.breed}
                    </td>
                    <td style="align-content: center">
                        ${pig.additionalInfo}
                    </td>
		                <td >
			                <a class="button btn-info  btn-lg"
			                   href="${createLink(action: 'delete', controller: 'pig', params: [id: "${pig.id}"])}"><g:message code="button.message.addToCart"/></a>
		                </td>
                </tr>
            </g:each>
        </table>
	    </g:if>
	    <g:paginate controller="pig" action="find" total="${pigCount}" max="${request.max}"
	                params="${[breed: pageScope.breed, searchQuery: pageScope.searchQuery]}"/>
      </div>
    </body>
</html>