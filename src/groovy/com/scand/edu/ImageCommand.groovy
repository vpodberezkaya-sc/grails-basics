package com.scand.edu

import grails.validation.Validateable
import org.springframework.web.multipart.MultipartFile

/**
 * Created by podberezkaya on 11/23/2018.
 */
@Validateable
class ImageCommand {

    MultipartFile imageFile

    static constraints = {
        imageFile validator: { MultipartFile file ->
            !file.empty
        }
    }

    String getFileName() {
        imageFile.originalFilename
    }
}
