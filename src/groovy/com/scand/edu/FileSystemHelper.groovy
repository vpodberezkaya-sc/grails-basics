package com.scand.edu

import grails.util.Holders

/**
 * Created by podberezkaya on 11/30/2018.
 */
class FileSystemHelper {
     String IMAGE_ROOT_PATH

     FileSystemHelper(){
        IMAGE_ROOT_PATH = Holders.config.uploadFile
    }
}
