package com.scand.edu

/**
 * Created by podberezkaya on 11/26/2018.
 */
interface Imageable {

    String getImageFolder()
}